"""En base a la aplicación desarrollada en la Sustitución de SQL por MongoDB,
debe sustituir el uso de MongoDB por el uso de Redis. """

import sys
import redis

REDISURL = 'localhost'

client = redis.Redis(REDISURL)

def slang_panameno():
    palabra = input("Digite la palabra del slang panameño: ")
    definicion = input("Digite la definicion de la palabra: ")
    client.set(palabra, definicion)
    print("La palabra fue agregada exitosamente")
    opciones()

def editar_palabra():
    print("Las palabras que pueden ser modificadas son: ")
    busq = client.scan_iter()
    for resultado in busq:
        print(resultado + ' - '.encode() + client.get(resultado))
    palabra = input("Escriba la palabra que desea editar: ")

    if client.get(palabra):
        nuevapalabra = input('Escriba la nueva palabra para -> ' + palabra + ':')
        client.rename(palabra, nuevapalabra)
        print("La palabra se ha editado corectamente")
        opciones()
    else:
        print('La palabra :' + palabra + ' no existe')

def eliminar_palabra():
    print("Las palabras que pueden ser eliminadas son: ")
    busq = client.scan_iter()
    for resultado in busq:
        print(resultado + ' - '.encode() + client.get(resultado))

    palabra = input("Escriba la palabra que desea eliminar: ")

    if client.get(palabra):
        print('La palabra a eliminar es: ' + palabra)
        client.delete(palabra)
        print("La palabra se ha eliminado correctamente")
        opciones()
    else:
        print('La palabra :' + palabra + ' no existe')

def ver_palabras():
    busq = client.scan_iter()
    for resultado in busq:
        print(resultado + ' - '.encode() + client.get(resultado))
    opciones()

def buscar_significado():
    palabra = input ("Escriba la palabra que desea buscar: ")

    if client.get(palabra):
        print('Significado de ' + palabra + ' es ' + str(client.get(palabra)))
    else:
        print('La palabra :' + palabra + ' no existe')
    opciones()

def opciones():
    print("Bienvenidos al slang panemeño en la base de datos Redis")
    print("1- Agregar nueva palabra")
    print("2- Editar palabra existente")
    print("3- Eliminar palabra existente")
    print("4- Ver listado de palabras")
    print("5- Buscar significado de palabras")
    print("6- Salir")

    opcion = input("Seleccione una opcion:")

    if opcion == '1':
        slang_panameno()
    elif opcion == '2':
        editar_palabra()
    elif opcion == '3':
        eliminar_palabra()
    elif opcion == '4':
        ver_palabras()
    elif opcion == '5':
        buscar_significado()
    elif opcion == '6':
        print("Regrese pronto")
        sys.exit()
    else:
        input("Seleccione una opcion valida !!!")
        opciones()

opciones()